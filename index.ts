const canvas = document.getElementById('canvas') as HTMLCanvasElement
const colorPicker = document.getElementById('color-picker') as HTMLInputElement

canvas.width = window.innerWidth - 4
canvas.height = window.innerHeight - 4

const ctx = canvas.getContext('2d')

ctx.strokeStyle = '#ff8100'
ctx.lineWidth = 4

colorPicker.value = ctx.strokeStyle

colorPicker.addEventListener('change', e => {
  const target = e.target as HTMLInputElement
  ctx.strokeStyle = target.value
})

const touches = {}

canvas.addEventListener('touchstart', (e) => {
  const touch = e.changedTouches[0]
  touches[touch.identifier] = { clientX: touch.clientX, clientY: touch.clientY }
}, false)

document.body.addEventListener('touchmove', (e) => {
  e.preventDefault()
})

canvas.addEventListener('touchmove', (e) => {
  for (let i = 0; i < e.changedTouches.length; ++i) {
    const touch = e.changedTouches[i]
    const oldTouch = touches[touch.identifier]
    ctx.beginPath()
    ctx.moveTo(oldTouch.clientX, oldTouch.clientY)
    ctx.lineTo(touch.clientX, touch.clientY)
    ctx.stroke()
    ctx.closePath()
    oldTouch.clientX = touch.clientX
    oldTouch.clientY = touch.clientY
  }
}, false)

canvas.addEventListener('touchend', (e) => {
  const touch = e.changedTouches[0]
  delete touches[touch.identifier]
}, false)
